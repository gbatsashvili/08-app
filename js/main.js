// language
$(function () {
    $("#language-switch").change(function () {
        if ($('#language-switch').is(':checked')) {
            $("#eng").addClass("active-lang");
            $("#geo").removeClass("active-lang");


        } else {
            $("#geo").addClass("active-lang");
            $("#eng").removeClass("active-lang");

        }



    });


    // range slider
    var RangeSlider = document.getElementById('range-slider');
    var gel = "<span>e</span>";
    noUiSlider.create(RangeSlider, {
        start: [5, 20],
        connect: true,
        tooltips: true,
        range: {
            'min': 5,
            'max': 20
        },
        format: {
            from: function (value) {
                return parseInt(value);
            },
            to: function (value) {
                return parseInt(value) + gel;
            }
        }

    });


});

//input restriction

$('.valuta-convertor input').keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode === 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39))
        return;

    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57 || e.keyCode == 43)) && (e.keyCode < 96 || e.keyCode > 105))
        e.preventDefault();
});

// star rating
var $star_rating = $('.star-rating .fa');

var SetRatingStar = function () {
    return $star_rating.each(function () {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
};


$star_rating.on('click', function () {
    $star_rating.siblings('input.rating-value').val($(this).data('rating'));
    return SetRatingStar();
});

SetRatingStar();
$(document).ready(function () {

});

// check all logic
$(document).ready(function () {
    $("#alldest").change(function () {
        if (this.checked) {
            $(".checkSingle").each(function () {
                this.checked = true;
            });
        } else {
            $(".checkSingle").each(function () {
                this.checked = false;
            });
        }
    });

    $(".checkSingle").click(function () {
        if ($(this).is(":checked")) {
            var isAllChecked = 0;

            $(".checkSingle").each(function () {
                if (!this.checked)
                    isAllChecked = 1;
            });

            if (isAllChecked == 0) {
                $("#alldest").prop("checked", true);
            }
        } else {
            $("#alldest").prop("checked", false);
        }
    });
});
